import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;


public class AddSonActionListener implements ActionListener, TreeOperator {
	TreePath treePath;
	FileTree fileTree;
	
	AddSonActionListener (FileTree _fileTree) {
		fileTree = _fileTree;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (treePath==null) {
			return;
		}
		DefaultMutableTreeNode node =  (DefaultMutableTreeNode)treePath.getLastPathComponent();
		
		if (!(node.getUserObject() instanceof File)) {
			return;
		}
		
		File file = (File)node.getUserObject();
		
		if (file.isDirectory()) {
			node.add(new DefaultMutableTreeNode("son New!!"));
		}
		fileTree.reload();
	}

	@Override
	public void add(TreePath _treePath) {
		treePath = _treePath;
	}
}
