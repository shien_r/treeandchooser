import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;


public class AddBroActionListener implements ActionListener, TreeOperator {
	TreePath treePath;
	FileTree fileTree;
	
	AddBroActionListener (FileTree _fileTree) {
		fileTree = _fileTree;
	}
	
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (treePath==null) {
			return;
		}
		
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)treePath.getLastPathComponent();
		
		if (node.isRoot()) {
			return;
		}
		DefaultMutableTreeNode parent = (DefaultMutableTreeNode)node.getParent();
		parent.add(new DefaultMutableTreeNode("new Bro File"));
		System.out.println(parent.getChildCount());
		fileTree.reload();
	}

	@Override
	public void add(TreePath _treePath) {
		treePath = _treePath;
	}
}
