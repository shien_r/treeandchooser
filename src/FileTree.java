import java.awt.Component;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JTree;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class FileTree extends JTree {
	DefaultTreeModel treeModel;

	FileTree (Component _component) {
		JFileChooser filechooser = new JFileChooser();
		filechooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int selected = filechooser.showOpenDialog(_component);
		
		// オープンできたら
		if (selected == JFileChooser.APPROVE_OPTION) {
			File file = filechooser.getSelectedFile();
			this.changeRootFile(file);
		} else if (selected == JFileChooser.CANCEL_OPTION) {
		
		} else if (selected == JFileChooser.ERROR_OPTION) {
		
		}
	}
	
	public void reload() {
		treeModel.reload();
	}
	
	public void changeRootFile(File _file) {
		DefaultMutableTreeNode root = new DefaultMutableTreeNode();
		this.treeModel = new DefaultTreeModel(root);
		this.readUnderRootNode(_file, root);
		this.configureJTree(this.treeModel);
	}

	private void readUnderRootNode(File _file, DefaultMutableTreeNode root) {
		// ホームディレクトリをセットする。それ以下を読み込むよ
		File fileRoot = _file;
		// ルート直下のファイルの読み込みとノードに登録
		for (File fileSystemRoot : fileRoot.listFiles()) {
			DefaultMutableTreeNode node = new DefaultMutableTreeNode(
					fileSystemRoot);
			root.add(node);
			makeFileNode(fileSystemRoot, node);
		}
	}
	
	private void makeFileNode(File fileSystemRoot, DefaultMutableTreeNode node) {
		FileSystemView fileSystemView = FileSystemView.getFileSystemView();
		for (File file : fileSystemView.getFiles(fileSystemRoot, true)) {
			if (file.isDirectory()) {
				DefaultMutableTreeNode newNode = new DefaultMutableTreeNode(file); 
				System.out.println(file.getName());
				node.add(newNode);
				makeFileNode(file, newNode);
			} else {
				node.add(new DefaultMutableTreeNode(file));
			}
		}
	}

	private void configureJTree(DefaultTreeModel _defaultTreeModel) {
		FileSystemView fileSystemView = FileSystemView.getFileSystemView();
		// createEmptyBorder:上下左右の辺の幅を指定して、スペースをとるが、描画を行わない空のボーダーを生成
		setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
		setRootVisible(true);
		// 一段下のファイルの処理を書く
		addTreeWillExpandListener(new FolderWillExpandListener(fileSystemView));
		// 各ノードの見た目を設定するためのやつを登録s
		setCellRenderer(new FileTreeCellRenderer(getCellRenderer(),
				fileSystemView));
		// expandRow:指定された行にあるノードが展開され、表示可能になるように 0行目を指定
		expandRow(0);
		setModel(_defaultTreeModel);
	}
}
