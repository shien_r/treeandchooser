import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JLabel;


public class OpenDialogListener implements ActionListener {
	Component component;
	JLabel jLabel;
	FileTree fileTree;
	
	OpenDialogListener (Component _component, JLabel _jLabel, FileTree _fileTree) {
		component = _component;
		jLabel = _jLabel;
		fileTree = _fileTree;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser filechooser = new JFileChooser();
		filechooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int selected = filechooser.showOpenDialog(component);
		
		// オープンできたら
		if (selected == JFileChooser.APPROVE_OPTION) {
			File file = filechooser.getSelectedFile();
			jLabel.setText(file.getAbsolutePath());
			fileTree.changeRootFile(file);
		} else if (selected == JFileChooser.CANCEL_OPTION) {
			jLabel.setText("キャンセル");
		} else if (selected == JFileChooser.ERROR_OPTION) {
			jLabel.setText("エラーとか取り消し");
		}
		
	}
}
