import java.awt.Color;
import java.awt.Component;
import java.io.File;

import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;

import javax.swing.JLabel;
import javax.swing.JTree;
import javax.swing.tree.TreeCellRenderer;


/* Tree に表示される各ノードの見た目とか。名前にJLabel をつかっている。
 * Icon 指定可能
 * */

public class FileTreeCellRenderer extends DefaultTreeCellRenderer {
    private final TreeCellRenderer renderer;
    private final FileSystemView fileSystemView;
    
	FileTreeCellRenderer (TreeCellRenderer _renderer, FileSystemView _fileSystemView) {
		this.renderer = _renderer;
		this.fileSystemView = _fileSystemView;
	}
	@Override
	public Component getTreeCellRendererComponent(JTree tree, Object value,
			boolean selected, boolean expanded, boolean leaf, int row,
			boolean hasFocus) {
		JLabel c = (JLabel) renderer.getTreeCellRendererComponent(tree, value,
				selected, expanded, leaf, row, hasFocus);
		configureSelectedNode(selected, c);
		if (value instanceof DefaultMutableTreeNode) {
			DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
			Object obj = node.getUserObject();
			configLabel(c, obj);
		}
		return c;
	}
	
	private void configureSelectedNode(boolean selected, JLabel jLabel) {		
		jLabel.setOpaque(!selected);
		Color color = (selected) ? getTextSelectionColor() : getTextNonSelectionColor();
        jLabel.setForeground(color);
		if(!selected) {
            jLabel.setBackground(getBackgroundNonSelectionColor());
        }
	}
	
	private void configLabel(JLabel jlabel, Object object) {
		if(!(object instanceof File)) {
			return;
		}
        File file = (File)object;
        jlabel.setIcon(fileSystemView.getSystemIcon(file));
        jlabel.setText(fileSystemView.getSystemDisplayName(file));
        jlabel.setToolTipText(file.getPath());
	}

}
