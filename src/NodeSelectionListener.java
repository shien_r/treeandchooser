import java.util.ArrayList;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.TreePath;


public class NodeSelectionListener implements TreeSelectionListener {

	private JTree jTree;
	private ArrayList<TreeOperator> treeOperators;
	
	NodeSelectionListener(JTree _jTree) {
		jTree = _jTree; 
		treeOperators = new ArrayList<TreeOperator>();
	}
	
	public void addActionListener(TreeOperator _treeOperator) {
		treeOperators.add(_treeOperator);
	}
	
	@Override
	public void valueChanged(TreeSelectionEvent e) {
	    TreePath path = jTree.getSelectionPath();
	    for (TreeOperator treeOperator: treeOperators) {
	    	treeOperator.add(path);
	    }
	}

}
