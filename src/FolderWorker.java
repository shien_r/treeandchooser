import java.io.File;
import java.util.List;

import javax.swing.SwingWorker;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class FolderWorker extends SwingWorker<String,File> {
	private FileSystemView fileSystemView;
	private DefaultMutableTreeNode defaultMutableTreeNode;
	private File file;
	private DefaultTreeModel defaultTreeModel;
	
	FolderWorker (FileSystemView _fileSystemView,
			DefaultMutableTreeNode _defaultMutableTreeNode,
			File _file,
			DefaultTreeModel _defaultTreeModel) {
		fileSystemView = _fileSystemView;
		defaultMutableTreeNode = _defaultMutableTreeNode;
		file = _file;
		defaultTreeModel = _defaultTreeModel;
		
	}
	
	@Override
	public String doInBackground() throws Exception {
		File[] children = (File[])fileSystemView.getFiles(file, true);
        for(File child: children){
        	publish(child);
        }
        return "Data";
	}
	
	@Override
    public void process(List<File> chunks){
        for(File file: chunks){
            defaultMutableTreeNode.add(new DefaultMutableTreeNode(file));
        }
        defaultTreeModel.nodeStructureChanged(defaultMutableTreeNode);
    }

}
