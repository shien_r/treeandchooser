import java.io.File;
import javax.swing.JTree;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeWillExpandListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.ExpandVetoException;

/* ルート直下のディレクトリより更に下のファイルの登録と整理。
 * SwingWorker によって別スレッドに任せる。
 * 
 */
public class FolderWillExpandListener implements TreeWillExpandListener {

	private final FileSystemView fileSystemView;
	 
    public FolderWillExpandListener(FileSystemView fileSystemView){
        this.fileSystemView = fileSystemView;
    }    
    
    
    // 処理を小分けにするためだ……なるほど
    // 最初に全て読み込んだら大変なことになる、特にホーム/ルートディレクトリとか
	@Override
	public void treeWillExpand(TreeExpansionEvent event)
			throws ExpandVetoException {
		

//        final JTree tree = (JTree) event.getSource();
//        final DefaultMutableTreeNode node =
//                (DefaultMutableTreeNode) event.getPath().getLastPathComponent();
//        final DefaultTreeModel model = (DefaultTreeModel) tree.getModel();
//        if (!(node.getUserObject() instanceof File)) {
//        	return;
//        }
//        final File parent = (File) node.getUserObject();
//        
//        if(!parent.isDirectory())
//            return;
// 
//        for(int i=0; i<node.getChildCount(); i++){
//            final DefaultMutableTreeNode newnode = (DefaultMutableTreeNode) node.getChildAt(i);
//            final File newFile = (File) newnode.getUserObject();
//            
//            if(newnode.getChildCount() > 0) {
//            	continue;
//            }
//            FolderWorker worker = new FolderWorker(fileSystemView, newnode, newFile, model);
//            worker.execute();
//        }
    }

	@Override
	public void treeWillCollapse(TreeExpansionEvent event)
			throws ExpandVetoException {
		// TODO Auto-generated method stub

	}

}
