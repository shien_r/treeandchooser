import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class IndicateDirPanel extends JPanel {
	IndicateDirPanel(FileTree _fileTree) {
		JButton jButton = new JButton("ダイアログを開く");
	    JLabel jLabel = new JLabel("ここに表示");
	    jButton.addActionListener(new OpenDialogListener(this,jLabel, _fileTree));
	    this.add(jButton);
	    this.add(jLabel);
	    this.setLayout(new GridLayout(2,1));
	}
}
