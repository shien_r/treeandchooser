
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;


public class TreeAndChooser extends JFrame {
	TreeAndChooser () {
		super();	    
	    FileTree fileTree = new FileTree(this);
	    NodeSelectionListener nodeSelectionListener = new NodeSelectionListener(fileTree);	    
	    DeleteActionListener delete = new DeleteActionListener(fileTree);
	    AddBroActionListener addBro = new AddBroActionListener(fileTree);
	    AddSonActionListener addSon = new AddSonActionListener(fileTree);

	    // Tree を選んだのちの処理
	    nodeSelectionListener.addActionListener(delete);
	    nodeSelectionListener.addActionListener(addBro);
	    nodeSelectionListener.addActionListener(addSon);
		
		// Tree を選んだ時の Listener
		fileTree.addTreeSelectionListener(nodeSelectionListener);
	    IndicateDirPanel indicateDirPanel = new IndicateDirPanel(fileTree);
	    this.add(indicateDirPanel);
	    NodeOperationPanel nodeOperationPanel = new NodeOperationPanel();
	    
	    nodeOperationPanel.addDeleteActionListener(delete);
	    nodeOperationPanel.addAddBroActionListener(addBro);
	    nodeOperationPanel.addAddSonActionListener(addSon);

	    this.add(nodeOperationPanel);
	    this.add(new JScrollPane(fileTree));
	    this.setLayout(new GridLayout(2,2));
		this.setVisible(true);
		this.setSize(500, 500);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new TreeAndChooser();
	}
}