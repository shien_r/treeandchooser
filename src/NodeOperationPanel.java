import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class NodeOperationPanel extends JPanel {
	JButton addSonNode;
	JButton addBroNode;
	JButton delNode;
	NodeOperationPanel() {
		this.setSize(50,100);
		addSonNode = new JButton("子ノード追加");
		addBroNode = new JButton("兄弟ノード追加");
		delNode = new JButton("選択ノード消去");
		this.add(addSonNode);
		this.add(addBroNode);
		this.add(delNode);
	}
	
	public void addDeleteActionListener(DeleteActionListener deleteActionListener) {
		delNode.addActionListener(deleteActionListener);
	}
	
	public void addAddBroActionListener(AddBroActionListener addBroActionListener) {
		addBroNode.addActionListener(addBroActionListener);
	}
	
	public void addAddSonActionListener(AddSonActionListener addSonActionListener) {
		addSonNode.addActionListener(addSonActionListener);
	}
}
