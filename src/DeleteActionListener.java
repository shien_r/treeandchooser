import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;


public class DeleteActionListener implements ActionListener, TreeOperator {
	TreePath treePath;
	FileTree fileTree;
	
	DeleteActionListener (FileTree _fileTree) {
		fileTree = _fileTree;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (treePath==null) {
			return;
		}
		DefaultMutableTreeNode node =  (DefaultMutableTreeNode)treePath.getLastPathComponent();
		node.removeAllChildren();
		fileTree.reload();
	}

	@Override
	public void add(TreePath _treePath) {
		treePath = _treePath;
	}
}
